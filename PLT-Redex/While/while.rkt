#lang racket
(require redex)

;;;;;Language;;;;;
(define-language While
  ;Main language specification
  (exp ::= a b)
  (a ::=
     n
     x
     (a ...)
     (a a-op a))
  (a-op ::= + - *)
  (b ::=
     bool
     x
     (b ...)
     (a b-op1 a)
     (b b-op2 b)
     (b-op3 b))
  (b-op1 ::= = <=)
  (b-op2 ::= &)
  (b-op3 ::= not)
  (S ::=
     Dp
     (S ...)
     (if b then W else W)
     (while b do W)
     
     (x := exp)
     (var t x := exp)
     
     ;(decl Dv W end) ;Temporary
     
     (begin Dv Dp W end)
     (call x)
     
     (W par W)
     (protect W end)
     (Cp par W)
     (W par Cp))
  
  ;Reduction relation domain
  (W ::=
     exp;For typing testing only
     S
     cons
     Scope
     (W W) ;Composition
     Cp)
  
  ;Auxiliary parallelism operations
  (Cp ::=
      (protected W end))
  
  ;Scope operations
  (Scope ::=
         begin-scope
         end-scope)
  ;Block declarations
  (Dv ::=
      (Dv ...)
      (var t x := exp));S must be an arithemic or a boolean expression
  (Dp ::=
      (Dp ...)
      (proc x is W))
  
  ;Values
  (val ::= n bool)
  (n ::= natural)
  (bool ::= true false)
  (x ::= variable-not-otherwise-mentioned)
  (cons ::=
        void
        (void ...))
  
  ;Environments
  (σ ::= σ-v σ-p)
  (env ::= env-v env-p)
  (σ-v ::= ((x val) ...) )
  (env-v ::= (σ-v ...))
  (σ-p ::= ((x S) ...) )
  (env-p ::= (σ-p ...))
  
  ;Context evaluation
  (E ::=
     (n a-op E)
     (E a-op a)
     
     (n b-op1 E)
     (E b-op1 a)
     
     (bool b-op2 E)
     (E b-op2 b)
     
     (b-op3 E)
     
     (E a);Necessary?
     (n E);Necessary?
     
     (E W ...)
     (E Dp W ...)
     
     (if E then W else W)
     (var t x := E)
     (x := E)
     (E par S)
     (S par E)
     (protected E end)
     hole)
  
  ;;;Typing;;;
  ;Types
  (t ::= Nat Bool Cmd)
  ;Typing environment
  (Γ ::= ((x t) ...))
  (δ ::= ((x Γ) ...))
  )
;;;;;Reduction Relations;;;;;
(define-metafunction While
  assign : env x any -> env
  [(assign (((x any) ...) σ ...)
           x_1 any_1) (((x_1 any_1) (x any) ...) σ ...)])

(define-metafunction While
  update : env x any -> env
  [(update (  σ_1 ... ((x_1 any_1) ... (x any_3) (x_2 any_2) ...) σ_2 ...) x any)
   (  σ_1 ... ((x_1 any_1) ... (x any) (x_2 any_2) ...) σ_2 ...)])

(define-metafunction While
  find : any x -> any
  [(find ( ((x_1 any_1) ... (x any_t) (x_2 any_2) ...) σ ...) x)
   any_t
   (side-condition (not (member (term x) (term (x_1 ...)))))]
  [(find ( σ_1 σ_2 ...) x)
   (find (σ_2 ...) x)]
  [(find any_1 any_2)
   ,(error 'find "not found: ~e in: ~e" (term any_1) (term any_2))])

(define-metafunction While
  free : any x -> any
  [(free
    ( ((x_1 any_1) ... (x any_t) (x_2 any_2) ...) σ ...)
    x)
   ( ((x_1 any_1) ... (x_2 any_2) ...) σ ...)
   
   (side-condition (not (member (term x) (term (x_1 ...)))))]
  [(free any_1 any_2) any_1])

(define-metafunction While
  new-scope : (σ ...) -> (σ ...)
  [(new-scope (σ ...))
   (() σ ...)])

(define-metafunction While
  free-scope : env -> env
  [(free-scope (σ_1 σ_2 ...))
   (σ_2 ...)]
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define s->βs
  (reduction-relation
   While
   #:domain (W env-v env-p)
   ;Composition
   (--> [(in-hole E (void W)) env-v env-p]
        [(in-hole E W) env-v env-p])
   ;Variables
   (--> [(in-hole E (var t x := val)) env-v env-p]
        [(in-hole E void) (assign (free env-v x) x val) env-p])
   (--> [(in-hole E (proc x is W)) env-v env-p];
        [(in-hole E void) env-v (assign env-p x W)])
   (--> [(in-hole E (call x)) env-v env-p];
        [(in-hole E (find env-p x)) env-v env-p])
   (--> [(in-hole E (x := val)) env-v env-p]
        [(in-hole E void) (update env-v x val) env-p])
   (--> [(in-hole E x) env-v env-p];
        [(in-hole E (find env-v x)) env-v env-p])
   ;Arithmetic expressions
   (--> [(in-hole E (n_1 + n_2)) env-v env-p]
        [(in-hole E ,(+ (term n_1) (term n_2))) env-v env-p])
   (--> [(in-hole E (n_1 - n_2)) env-v env-p]
        [(in-hole E ,(- (term n_1) (term n_2))) env-v env-p])
   (--> [(in-hole E (n_1 * n_2)) env-v env-p]
        [(in-hole E ,(* (term n_1) (term n_2))) env-v env-p])
   ;Boolean expressions
   (--> [(in-hole E (n_1 = n_2)) env-v env-p]
        [(in-hole E ,(if (equal? (term n_1) (term n_2)) (term true) (term false))) env-v env-p])
   (--> [(in-hole E (n_1 <= n_2)) env-v env-p]
        [(in-hole E ,(if (<= (term n_1) (term n_2)) (term true) (term false))) env-v env-p])   
   (--> [(in-hole E (bool_1 & bool_2)) env-v env-p]
        [(in-hole E ,(if (equal? (term bool_1) (term bool_2)) (term true) (term false))) env-v env-p])
   (--> [(in-hole E (not bool)) env-v env-p]
        [(in-hole E ,(if (equal? (term bool) true) (term false) (term true))) env-v env-p])
   ;Conditional clauses
   (--> [(in-hole E (if true then W_1 else W_2)) env-v env-p]
        [(in-hole E ,(term W_1)) env-v env-p])
   (--> [(in-hole E (if false then W_1 else W_2)) env-v env-p]
        [(in-hole E ,(term W_2)) env-v env-p])
   (--> [(in-hole E (while b do S)) env-v env-p]
        [(in-hole E ,(term (if b then ( S (while b do S)) else void))) env-v env-p])
   ;Blocks
   (--> [(in-hole E (begin Dv Dp W end)) env-v env-p]
        [(in-hole E ,(term (begin-scope (Dv (Dp (W end-scope)))))) env-v env-p])
   (--> [(in-hole E begin-scope) env-v env-p]
        [(in-hole E void) (new-scope env-v) (new-scope env-p)])
   (--> [(in-hole E end-scope) env-v env-p]
        [(in-hole E void) (free-scope env-v) (free-scope env-p)])
   ;Parallelism
   (--> [(in-hole E (W par cons)) env-v env-p]
        [(in-hole E ,(term W)) env-v env-p])
   (--> [(in-hole E (cons par W)) env-v env-p]
        [(in-hole E ,(term W)) env-v env-p])
   (--> [(in-hole E (protect W end)) env-v env-p]
        [(in-hole E ,(term (protected W end))) env-v env-p])
   (--> [(in-hole E (protected (cons S) end)) env-v env-p]
        [(in-hole E ,(term (protected S end))) env-v env-p])
   (--> [(in-hole E (protected cons end)) env-v env-p]
        [(in-hole E void) env-v env-p])
   ))

;(traces s->βs (term ( ( (var Nat x := 4) (x := 8) ) (()) (()) )))

;(traces s->βs (term ( ( (var Nat y := 4) (y := (y + 1)) ) (()) (()) )))

;(traces s->βs (term ( ( (var Bool x := true) (x := (not x))) (()) (()) )))

;(traces s->βs (term ( ( (var Bool x := (4 = 2)) (x := (not x)) ) (()) (()) )))

;(traces s->βs (term (( (var Nat x := 2) (if (x = 1) then (x := 1) else (x := 2)) (var Nat y := (x + 1))) (()) (()) )))

;(traces s->βs (term (( (var Bool y := false) (if (not y) then (var Nat z := 1) else (var Nat z  := 3))) (()) (()) )))

;(traces s->βs (term ((var Nat y := (x * 2)) (((x 4))) (()) )))

;(traces s->βs (term (((var Nat res := 10)(begin (var Nat x := 6) (proc r is (var Nat d := 9)) (begin (var Nat y := 8) (proc c is (var Nat d := 9)) (res := (x + y)) end) end)) (()) (()) )))

;(traces s->βs (term ((begin (var Nat w := 2) (proc z is (var Nat r := 4)) ((call z) (w := r)) end) (()) (()) )))

;(traces s->βs (term ((begin (var Nat x := 1) (proc r is (var Nat d := 1)) (var Nat y := (x + 6)) end) (()) (()) )))

;(traces s->βs (term (((var Nat res := 10) (begin (var Nat x := 6) (proc r is (var Nat d := 9)) ((call r) (res := (res + (d + x)))) end)) (()) (()) )))

;(traces s->βs (term (( (var Nat y := 0) (while (y = 0) do (y := (y + 1)))) (()) (()) )))

;(traces s->βs (term ((( ( ( var Nat x := 2) (var Nat x := 4) ) par ((var Nat x := 6)) ) ) (()) (()) )))

;(traces s->βs (term ((( ( ( var Nat x := 2) (var Nat x := 4) ) par ((var Nat x := 6)) ) ) (()) (()) )))

;(traces s->βs (term (((var Nat x := 0) ( ( protect (( x := 2) (x := 4)) end ) par (x := 6) )) (()) (()) )))

;(traces s->βs (term ( ((var Nat x := 0) ( ( protect ( (x := 2) par (x := 4) ) end   ) par ( (x := 6) (x := 8 )))) (()) (()) )))

;(traces s->βs (term ( ((var Nat x := 0) ( ( protect ( (x := 2) par (x := 4) ) end   ) par ( (x := 6) par (x := 8 )))) (()) (()) )))

;;;;;Typing;;;;;

(define-metafunction While
  assing-typing : Γ x t -> Γ
  [(assing-typing ((x_Γ t_Γ) ...) x t) ((x t) (x_Γ t_Γ) ...)])

(define-metafunction While
  assign-proc-vars : δ x Γ -> δ
  [(assign-proc-vars ((x_1 Γ_1) ...) x Γ) ((x Γ) (x_1 Γ_1) ...)])

(define-metafunction While
  free-typing : Γ x -> Γ
  [(free-typing
    ((x_1 any_1) ... (x any_t) (x_2 any_2) ...)
    x)
   ((x_1 any_1) ... (x_2 any_2) ...)
   
   (side-condition (not (member (term x) (term (x_1 ...)))))]
  [(free-typing any_1 any_2) any_1])

(define-metafunction While
  find-typing : Γ x -> t
  [(find-typing ((x_1 t_1) ... (x t) (x_2 t_2) ...) x)
   t
   (side-condition (not (member (term x) (term (x_1 ...)))))]
  [(find-typing any_1 any_2) ,(error 'lookup "not found: ~e" (term any_1))])

(define-metafunction While
  find-proc-vars : δ x -> Γ
  [(find-proc-vars ((x_1 Γ_1) ... (x Γ) (x_2 Γ_2) ...) x)
   Γ
   (side-condition (not (member (term x) (term (x_1 ...)))))]
  [(find-proc-vars any_1 any_2) ,(error 'lookup "not found: ~e" (term any_1))])

(define-metafunction While
  union : Γ Γ -> Γ
  [(union ((x_1 t_1) ... ) ((x_2 t_2) ...))
   ((x_1 t_1) ... (x_2 t_2) ...)])

(define-metafunction While
  subtract-set : Γ Γ -> Γ
  [(subtract-set ((x_1 t_1) ... ) ((x_2 t_2) ... (x_1 t_1) ...))
   ((x_2 t_2) ...)])

(module+ test
  (test-equal (term (union ((x Nat)) ((y Nat)))) (term ((x Nat) (y Nat)))))

(module+ test
  (test-equal (term (find-proc-vars ((q ((x Nat)))) q)) (term ((x Nat)) )))

(define-judgment-form
  While
  #:mode (⊢ I I I O O O)
  #:contract (⊢ Γ_1 δ_1 W Γ_2 δ_2 t)
  
  [--------------------
   (⊢ Γ δ n Γ δ Nat)]
  
  [--------------------
   (⊢ Γ δ bool Γ δ Bool)]
  
  [-----------------------
   (⊢ Γ δ x Γ δ (find-typing Γ x))]
  
  [(⊢ Γ δ a_1 Γ δ Nat) 
   (⊢ Γ δ a_2 Γ δ Nat)
   -----------------------
   (⊢ Γ δ (a_1 a-op a_2) Γ δ Nat)]
  
  [(⊢ Γ δ a_1 Γ δ Nat) 
   (⊢ Γ δ a_2 Γ δ Nat)
   -----------------------
   (⊢ Γ δ (a_1 b-op1 a_2) Γ δ Bool)]
  
  [(⊢ Γ δ b_1 Γ δ Bool) 
   (⊢ Γ δ b_2 Γ δ Bool)
   -----------------------
   (⊢ Γ δ (b_1 b-op2 b_2) Γ δ Bool)]
  
  [(⊢ Γ δ b Γ δ Bool) 
   -----------------------
   (⊢ Γ δ (b-op3 b) Γ δ Bool)]
  
  [(⊢ Γ δ exp Γ δ t_1)
   -----------------------
   (⊢ Γ δ (var t_1 x := exp) (assing-typing Γ x t_1) δ Cmd)]
  
  [(⊢ Γ δ x Γ δ t_1)
   (⊢ Γ δ exp Γ δ t_1)
   -----------------------
   (⊢ Γ δ (x := exp) Γ δ Cmd)]
  
  [(⊢ Γ_1 δ W_1 Γ_2 δ _)
   (⊢ Γ_2 δ W_2 Γ_3 δ _)
   -----------------------
   (⊢ Γ_1 δ (W_1 W_2) Γ_3 δ Cmd)]
  
  [(⊢ Γ_1 δ b Γ_1 δ Bool)
   (⊢ Γ_1 δ W_1 Γ_2 δ t)
   (⊢ Γ_1 δ W_2 Γ_3 δ t)
   -----------------------
   (⊢ Γ_1 δ (if b then W_1 else W_2) (union Γ_2 Γ_3) δ t)]
  
  [(⊢ Γ δ b Γ δ Bool)
   (⊢ Γ δ W Γ δ Cmd)
   -----------------------
   (⊢ Γ δ (while b do W) Γ δ Cmd)]
  
  [(⊢ Γ δ Dv Γ_1 δ Cmd)
   (⊢ Γ_1 δ Dp Γ_1 δ_1 Cmd)
   (⊢ Γ_1 δ_1 W Γ_2 δ_1 t)
   -----------------------
   (⊢ Γ δ (begin Dv Dp W end) Γ δ Cmd)]
  
  [(⊢ Γ δ W Γ_1 δ Cmd)
   -----------------------
   (⊢ Γ δ (proc x is W) Γ (assign-proc-vars δ x (subtract-set Γ Γ_1)) Cmd)]
  
  [-----------------------
   (⊢ Γ δ (call x) ( union (find-proc-vars δ x) Γ) δ Cmd)]
  
  [(⊢ Γ δ W_1 Γ_1 δ Cmd)
   (⊢ Γ_1 δ W_2 Γ_2 δ Cmd)
   -----------------------
   (⊢ Γ δ (W_1 par W_2) Γ_2 δ Cmd)]
  
  [(⊢ Γ δ W Γ_1 δ Cmd)
   -----------------------
   (⊢ Γ δ (protect W end) Γ_1 δ Cmd)]
  )

;(judgment-holds (⊢ () () ( (var Bool x := true) ( (begin (var Nat x := 2) (proc q is (var Nat y := 1)) (x := (x + 1)) end ) (x := true))) _ _ _))

;(judgment-holds (⊢ () () (begin (var Nat x := 2) (proc q is (var Nat y := 1)) ( (call q) (x + y)) end ) ((y Nat) (x Nat)) ((q ((y Nat)))) _))

;(judgment-holds (⊢ () () (begin (var Nat x := 2) (proc q is (var Nat y := 1)) ( (call q) (x + y)) end ) () () _))

;(judgment-holds (⊢ () () (begin (var Nat x := 2) (proc q is (var Nat y := 1)) ( (call q) (begin (var Nat z := 5) (proc r is (var Bool y := true)) (z := (y + x)) end)) end ) () () _))

;(judgment-holds (⊢ () () (begin (var Nat x := 2) (proc q is (var Nat y := 1)) (begin (var Nat z := 5) (proc r is (var Bool y := true)) (z := (1 + x)) end) end ) () () _))

;(judgment-holds (⊢ () () ( (var Nat x := 1) ( while (x <= 4) do (x := (x + 1)) )) _ _ _) )

;(judgment-holds (⊢ () () (if (not true) then (var Nat y := 2) else (var Nat y := 4)) _ _ _) )

;(judgment-holds (⊢ () () ( var Nat x := true) _ _ _) ) ;Supposed to return false

;(judgment-holds (⊢ () () ((var Nat y := 2) (not y)) _ _ _) ) ;Supposed to return false

;(judgment-holds (⊢ () ( ( (var Nat x := 1) ( (var Bool z := (x <= 4)) (if z then (var Nat z := (x * 2)) else (var Nat z := (x * 4))) )) ( (var Nat y := (x * 6)) (y - x))) _ _) )

;(judgment-holds (⊢ () () ((var Nat x := 4) ( (var Nat y := 3) (x + y) )) _ _ _) )

;(judgment-holds (⊢ () ( (var Bool x := #t) (if x then (var Nat y := 2) else (var Nat z := 6))) _ _))

;(judgment-holds (⊢ () ( (var Nat z := 5) (while (z <= 10) do (z := (z + 1)) )) _ _))

;(judgment-holds (⊢ () ( (var Bool x := #t) ( (decl ( (var Nat x := 1) ((var Bool z := #t) (var Bool y := #t))) ( (x + 1) (y := (not y))) end) (not x))) ((x Bool)) _))

;(judgment-holds (⊢ () (( (var Nat x := 1) (x := (x + 7))) par (var Nat x := 3)) ((x Nat)) _))

;(judgment-holds (⊢ () (protect ( (var Nat x := 1) (x := (x + 1))) end) ((x Nat)) _))